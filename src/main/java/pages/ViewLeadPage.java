package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods 
{
	public EditLeadPage clickEditbutton()

	{
		WebElement eleEditButton = locateElement("linktext", "Edit");
		click(eleEditButton);
		return new EditLeadPage();
	}
}

