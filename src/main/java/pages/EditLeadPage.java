package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods 
{
	public EditLeadPage editCompanyName(String data) 
	{

		WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");
		type(eleCompanyName, data);
		return this;
	}

	public EditLeadPage editFirstName(String data) 
	{

		WebElement eleFirstName = locateElement("id", "updateLeadForm_firstName");
		type(eleFirstName, data);
		return this;
	}

	public ViewLeadPage clickUpdateButton() 
	{

		WebElement eleUpdateButton = locateElement("name", "submitButton");
		click(eleUpdateButton);
		return new ViewLeadPage();
	}


}
