package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods 
{
	
	public FindLeadPage typeFirstname(String data)
	{
		WebElement eleFirstname = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(eleFirstname, data);
		return this;
	}
	
	public FindLeadPage clickFindLeadbutton()
	{
		WebElement eleFindLead = locateElement("linktext","Find Leads");
		click(eleFindLead);
		return this;
	}
	
	public ViewLeadPage clickFirstLeadName()
	{
		WebElement eleFirstLeadName = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[@class='linktext'])[1]");
		click(eleFirstLeadName);
		return new ViewLeadPage();
	}
}
